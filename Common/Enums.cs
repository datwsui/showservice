﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class Enums
    {
        #region Country
        public enum Country
        {
            AUS,
            UK,
            USA
        }
        #endregion
    }
}
