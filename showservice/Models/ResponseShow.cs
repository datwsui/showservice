﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace showservice.Models
{
    public class ResponseShow
    {
        public string image { get; set; }
        public string slug { get; set; }
        public string title { get; set; }
    }
}