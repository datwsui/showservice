﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace showservice.Models
{
    public class HttpRequest
    {
        public List<RequestShow> PayLoad { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public int TotalRecords { get; set; }
    }
}