﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace showservice.Models
{
    public class HttpResponse
    {
        public List<ResponseShow> response { get; set; }
        public string error { get; set; }
    }
}