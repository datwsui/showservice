﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;

namespace showservice.Models
{
    public class RequestShow
    {
        public Enums.Country Country { get; set; }
        public string Description { get; set; }
        public bool Drm { get; set; }
        public int EpisodeCount { get; set; }
        public string Genre { get; set; }
        public Image Image { get; set; }
        public string Language { get; set; }
        public Episode NextEpisode { get; set; }
        public List<Season> Seasons { get; set; }
        public string PrimaryColour { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public string TvChannel { get; set; }

        #region Methods
        public ResponseShow ConvertToResponse()
        {
            return new ResponseShow { image = this.Image.ShowImage,
                                      slug = this.Slug,
                                      title = this.Title
                                    };
        }
        #endregion
    }
}