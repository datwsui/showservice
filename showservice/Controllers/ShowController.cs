﻿using showservice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace showservice.Controllers
{
    public class ShowController : ApiController
    {
        [HttpPost]
        [Route("")]
        public HttpResponseMessage ShowFilter([FromBody] HttpRequest request)
        {
            HttpResponse response = new HttpResponse();
            try
            {
                //filter shows
                List<RequestShow> filteredShows = request.PayLoad.Where<RequestShow>(x => x.Drm && x.EpisodeCount > 0).ToList();
                
                #region build response
                List<ResponseShow> responseShows = new List<ResponseShow>();
                foreach (var show in filteredShows)
                {
                    responseShows.Add(show.ConvertToResponse());
                }
                response.response = responseShows;
                #endregion
            }
            catch (Exception e)
            {
                response.error = "Could not decode request: " + e.Message;
            }

            return Request.CreateResponse(string.IsNullOrEmpty(response.error) ? HttpStatusCode.OK : HttpStatusCode.BadRequest, response);
        }
    }
}
